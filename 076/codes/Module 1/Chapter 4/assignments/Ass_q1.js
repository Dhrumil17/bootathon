let n1 = document.getElementById('number1');
let n2 = document.getElementById('number2');
let answer = document.getElementById('answer');
function test_condition() {
    if (n1.value == "" && n2.value == "") {
        alert("please Enter Both Number");
    }
    else if (n1.value == "") {
        alert("please Enter Number1");
    }
    else if (n2.value == "") {
        alert("please Enter Number2");
    }
    else if (isNaN(parseFloat(n1.value)) || isNaN(parseFloat(n2.value))) {
        alert("please Enter Numbers..Not Alphabatic");
    }
    else {
        return 1;
    }
}
function addition() {
    let bool = test_condition();
    if (bool) {
        let ans = parseFloat(n1.value) + parseFloat(n2.value);
        answer.value = ans.toString();
    }
}
function subtraction() {
    let bool = test_condition();
    if (bool) {
        let ans = parseFloat(n1.value) - parseFloat(n2.value);
        answer.value = ans.toString();
    }
}
function multiplication() {
    let bool = test_condition();
    if (bool) {
        let ans = parseFloat(n1.value) * parseFloat(n2.value);
        answer.value = ans.toString();
    }
}
function division() {
    let bool = test_condition();
    if (parseFloat(n2.value) == 0) {
        alert("Division By 0 is not possible");
    }
    else {
        if (bool) {
            let ans = parseFloat(n1.value) / parseFloat(n2.value);
            answer.value = ans.toString();
        }
    }
}
//# sourceMappingURL=Ass_q1.js.map