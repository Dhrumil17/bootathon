/* Typescript code for print the user choice table */
function TABLE() {
    /* Take the value Entered by user*/
    let tbl_num = document.getElementById('num');
    let table = document.getElementById('table');
    let number = +tbl_num.value;
    /* Condition Whether user leave emptybox or not  */
    if (tbl_num.value == "") {
        alert('PLEASE ENTER VALUE');
    }
    
    else if(parseInt(tbl_num.value) <= 0){
        alert("please input positive number  ");
    }


    /* Condition Whether user input invalid or not */
    else if (isNaN(parseFloat(tbl_num.value))) {
        alert("PLEASE ENTER ONLY NUMBER");
    }
    else {
        table.border = "2";
        /* Condition for Heading */
        while (table.rows.length > 1) {
            table.deleteRow(1);
        }
        /* for loop for creating table */
        for (let i = 1; i <= number; i++) {
            var row = table.insertRow(); /*Add Row In table */
            var column = row.insertCell(); /*Add Column in that row */
            var textbox = document.createElement('input'); /* Create input element */
            textbox.type = "text";
            textbox.style.textAlign = "center"; /* css with typescript */
            textbox.style.height = "30px";
            textbox.style.fontSize = "20px";
            textbox.value = (number + "    *   " + i).toString();
            column.appendChild(textbox); /*append textbox in cell */
            var column1 = row.insertCell();
            column1.style.height = "30px";
            var textbox = document.createElement('input');
            textbox.type = "text";
            textbox.style.height = "30px";
            textbox.style.textAlign = "center";
            textbox.style.fontSize = "20px";
            textbox.style.fontWeight = "bold";
            textbox.style.backgroundColor = "lightblue";
            textbox.value = (number * i).toString();
            column1.appendChild(textbox);
        }
    }
}
//# sourceMappingURL=R2_q2.js.map