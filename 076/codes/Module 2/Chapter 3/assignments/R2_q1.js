// JAVA Code for Check whether a given point  
// lies inside a triangle or not 
/* Take the value Entered by user*/
let xx1 = document.getElementById('X1');
let yy1 = document.getElementById('Y1');
let xx2 = document.getElementById('X2');
let yy2 = document.getElementById('Y2');
let xx3 = document.getElementById('X3');
let yy3 = document.getElementById('Y3');
let pp1 = document.getElementById('P1');
let pp2 = document.getElementById('P2');
let display = document.getElementById('display');
/* A utility function handle the exception like empty textbox
,valid input etc... */
function test_condition() {
    if (xx1.value == "" || yy1.value == "" || xx2.value == "" || yy2.value == "" || xx3.value == "" || yy3.value == "" || pp1.value == "" || pp2.value == "") {
        alert("Please Enter All vertices");
    }
    else if (isNaN(parseFloat(xx1.value)) || isNaN(parseFloat(yy1.value)) || isNaN(parseFloat(xx2.value)) || isNaN(parseFloat(yy2.value)) || isNaN(parseFloat(xx3.value)) || isNaN(parseFloat(yy3.value)) || isNaN(parseFloat(pp1.value)) || isNaN(parseFloat(pp2.value))) {
        alert("Please Enter valid Input");
    }
    else {
        return true;
    }
}
/* A utility function to calculate area of triangle
formed by (x1, y1) (x2, y2) and (x3, y3) */
function area(s1, t1, s2, t2, s3, t3) {
    let ans = (s1 * (t2 - t3) + s2 * (t3 - t1) + s3 * (t1 - t2));
    return (Math.abs(ans) / 2);
}
/* Let us check whether the point P(p1, p2)
lies inside the triangle or not */
function check() {
    let bool = test_condition(); /* Exception handling */
    if (bool) {
        /* Parse the string value into number */
        let x1 = +xx1.value;
        let y1 = +yy1.value;
        let x2 = +xx2.value;
        let y2 = +yy2.value;
        let x3 = +xx3.value;
        let y3 = +yy3.value;
        let p1 = +pp1.value;
        let p2 = +pp2.value;
        /* Calculate area of triangle XYZ */
        let XYZ = (area(x1, y1, x2, y2, x3, y3));
        /* Calculate area of triangle PAB */
        let PAB = (area(p1, p2, x1, y1, x2, y2));
        /* Calculate area of triangle PBA */
        let PBC = (area(x2, y2, p1, p2, x3, y3));
        /* Calculate area of triangle PAC */
        let PAC = (area(x1, x2, x3, y3, p1, p2));
        let sum = PAB + PBC + PAC;
        /* Check if (sum of PAB, PAC and PBA)  = XYZ or not */
        if (sum == XYZ) { //if((xyz - sum) < 0.000000001)
            display.innerHTML = "<b>Point Lie <b>inside</b> XYZ Triangle</b>";
        }
        else {
            display.innerHTML = "<b>Point Lie <b>outside</b> XYZ Triangle</b>";
        }
    }
}
//# sourceMappingURL=R2_q1.js.map