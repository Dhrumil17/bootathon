// Typescript Code for Check whether a given point  
// lies inside a triangle or not 

/* Take the value Entered by user*/
let xx1:HTMLInputElement = <HTMLInputElement>document.getElementById('X1');
let yy1:HTMLInputElement = <HTMLInputElement>document.getElementById('Y1');

let xx2:HTMLInputElement = <HTMLInputElement>document.getElementById('X2');
let yy2:HTMLInputElement = <HTMLInputElement>document.getElementById('Y2');

let xx3:HTMLInputElement = <HTMLInputElement>document.getElementById('X3');
let yy3:HTMLInputElement = <HTMLInputElement>document.getElementById('Y3');

let pp1:HTMLInputElement = <HTMLInputElement>document.getElementById('P1');
let pp2:HTMLInputElement = <HTMLInputElement>document.getElementById('P2');

let display:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById('display');

/* A utility function handle the exception like empty textbox
,valid input etc... */
function test_condition(){
    if(xx1.value == "" || yy1.value == "" ||xx2.value == "" || yy2.value == "" ||xx3.value == "" || yy3.value == "" ||pp1.value == "" || pp2.value == ""  ){
        alert("Please Enter All vertices")
    }
    else if(isNaN(parseFloat(xx1.value)) || isNaN(parseFloat(yy1.value))||isNaN(parseFloat(xx2.value)) || isNaN(parseFloat(yy2.value))||isNaN(parseFloat(xx3.value)) || isNaN(parseFloat(yy3.value)) || isNaN(parseFloat(pp1.value)) || isNaN(parseFloat(pp2.value))){
        alert("Please Enter valid Input")
    }
    else {
        return true;
    }
}

/* A utility function to calculate area of triangle  
formed by (x1, y1) (x2, y2) and (x3, y3) */
function area(s1: number, t1: number, s2: number, t2: number, s3: number, t3: number): number{
    let ans = (s1*(t2-t3) + s2*(t3-t1) + s3*(t1-t2));
    return (Math.abs(ans)/2);
}

/* Let us check whether the point P(p1, p2) 
lies inside the triangle or not */
function check(){
    let bool = test_condition() /* Exception handling */
    if(bool){
        /* Parse the string value into number */
        let x1:number = +xx1.value;
        let y1:number = +yy1.value;
        let x2:number = +xx2.value;
        let y2:number = +yy2.value;
        let x3:number = +xx3.value;
        let y3:number = +yy3.value;
        let p1:number = +pp1.value;
        let p2:number = +pp2.value;

        /* Calculate area of triangle XYZ */
        let XYZ = (area(x1,y1,x2,y2,x3,y3));

        /* Calculate area of triangle PAB */
        let PAB = (area(p1,p2,x1,y1,x2,y2));

        /* Calculate area of triangle PBA */
        let PBC = (area(x2,y2,p1,p2,x3,y3));

        /* Calculate area of triangle PAC */
        let PAC = (area(x1,x2,x3,y3,p1,p2));
        
        let sum = PAB + PBC + PAC;

        /* Check if (sum of PAB, PAC and PBA)  = XYZ or not */
        if(sum == XYZ)   {                //if((xyz - sum) < 0.000000001)
            display.innerHTML = "<b>Point P Lie <b>inside</b> XYZ Triangle</b>"
        }
        else{
            display.innerHTML = "<b>Point P Lie <b>outside</b> XYZ Triangle</b>"
        }
    }
}